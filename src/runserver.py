"""
This script runs the Flask application using a development server.
"""

from os import environ
from cdx_api import app    # pylint: disable=E0401
# Note: take care PYTHONPATH is set correctly

if __name__ == '__main__':
    HOST = environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(environ.get('SERVER_PORT', '19888'))
    except ValueError:
        PORT = 19888
    app.run(HOST, PORT, debug = True)
